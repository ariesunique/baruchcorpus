from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField
from wtforms.fields import TextAreaField, HiddenField
from wtforms.validators import DataRequired, ValidationError, DataRequired, EqualTo, Email, Optional
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from app.models import User, Demographics, AccessLevel, CountryLookup, LanguageLookup, MajorLookup, PapertypeLookup


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Login')


class RegistrationForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[Email(), Optional()])
    access_level = QuerySelectField(query_factory=lambda: AccessLevel.query.order_by(AccessLevel.name).all(), get_label="description", validators=[DataRequired()])
    user_type = SelectField('User Type', choices=[('S', 'Student'), ('T', 'Teacher')])
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register User')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please choose a different username.')


class DemographicsForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[Email(), Optional()])
    country = QuerySelectField(query_factory=lambda: CountryLookup.query.order_by(CountryLookup.name).all(), get_label="name", validators=[DataRequired()])
    language = QuerySelectField(query_factory=lambda: LanguageLookup.query.order_by(LanguageLookup.name).all(), get_label="name", validators=[DataRequired()])
    major = QuerySelectField(query_factory=lambda: MajorLookup.query.order_by(MajorLookup.name).all(), get_label="name", validators=[DataRequired()])
    submit = SubmitField('Submit')


class StudentListForm(FlaskForm):
    name = QuerySelectField(query_factory=lambda: Demographics.query.all(), get_pk = lambda d: d.id, get_label= lambda d: d.student.last_name + ", " + d.student.first_name)
    submit = SubmitField('Submit')


class CorpusForm(FlaskForm):
    demographicid = HiddenField()
    title = StringField('Title', validators=[DataRequired()])
    text = TextAreaField('Full Text')
    draft = SelectField('Draft', choices=[('1', 'first'), ('2', 'second'), ('3', 'final')])
    papertype = QuerySelectField(query_factory=lambda: PapertypeLookup.query.order_by(PapertypeLookup.name).all(), get_label="name", validators=[DataRequired()])
    original = FileField('Upload your paper')
    submit = SubmitField('Submit')

    def validate_text(self, text):
        if not text.data.strip() and not self.original.data:
            raise ValidationError('Please upload a file, or enter text in the "Full Text" field.')

    def validate_demographicid(self, demographicid):
        if not demographicid.data:
            raise ValidationError('Please select a student from the top drowndown and click "Submit" before uploading your file')
