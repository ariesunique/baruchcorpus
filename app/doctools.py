import docx
import re
import os
from textblob import TextBlob
from werkzeug.utils import secure_filename


def doctools(filename=None, text=None, first=None, last=None, professor=None):
    if not (filename or text):
        raise Exception("ERROR: you must specify either a file or some raw text")

    if filename:
        #filename = os.path.join(path, 'uploads', file)
        #filename = secure_filename(file.filename)
        if not filename.endswith('.docx'):
            raise Exception("ERROR: file must be a word doc")

        print(f"processing {filename}")
        doc = docx.Document(filename)
        full_text = "\n\n".join([p.text for p in doc.paragraphs])
        full_text = str(full_text.encode('ascii', 'ignore'))

        # remove name
        if first and last:
            pattern = first + " " + last
            full_text = re.sub(pattern, '{{Name}}', full_text, flags=re.IGNORECASE)

            pattern = last + ", ?" + first
            full_text = re.sub(pattern, '{{Name}}', full_text, flags=re.IGNORECASE)

        # remove professor name
        if professor:
            pattern = r'(Dr\.) ?' + professor
            full_text = re.sub(pattern, '{{Professor}}', full_text, flags=re.IGNORECASE)

        # remove references at the end of the paper
        full_text = full_text[:full_text.rfind('References')]

        # remove citations
        pattern = r'\(([\w\s]+, ?\d{4};?)+\)'
        full_text = re.sub(pattern, '{{Citation}}', full_text)

        # count words (without the tags we added)
        pattern = r'\{\{\w+\}\}'
        counttext = re.sub(pattern, '', full_text)
        blob = TextBlob(counttext)
        #print(f"Words: {len(blob.words)}\nSentences: {len(blob.sentences)}")

        return full_text, len(blob.words)


