import os

from app import app, db, doctools
from app.models import Corpus, User, AccessLevel, Demographics
from app.forms import LoginForm, RegistrationForm, DemographicsForm, CorpusForm, StudentListForm

from flask import render_template, flash, redirect, request, url_for
from flask_login import current_user, login_user, logout_user, login_required

from werkzeug.urls import url_parse
from werkzeug.utils import secure_filename


# access levels
STUDENT_ACCESS = 1
TEACHER_ACCESS = 2
ADMIN_ACCESS = 3
SUPER_ACCESS = 4

# user types
TEACHER = "T"
STUDENT = "S"


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password', 'danger')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login_or_register.html', title='Login', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated and check_access(current_user, SUPER_ACCESS):
        form = RegistrationForm()
        if form.validate_on_submit():
            user = User(username=form.username.data, email=form.email.data,
                        first_name=form.first_name.data, last_name=form.last_name.data,
                        user_type=form.user_type.data, access=form.access_level.data)
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
            flash(f'You have successfully registered {user.username}', 'info')
            return redirect(url_for('index'))
        return render_template('login_or_register.html', title='Register', form=form)
    flash(f'You do not have access to register users.', 'warning')
    return redirect(url_for('index'))


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/corpus/add', methods=['GET', 'POST'])
@login_required
def corpus_add():
    # level2, nicknamed Teacher, can only query the corpus, but not add
    if int(current_user.access.name) == int(TEACHER_ACCESS):
        flash(f'You do not have access to add to the corpus.', 'warning')
        return redirect(url_for('index'))

    form = CorpusForm()

    if current_user.user_type == STUDENT:
        demographics = Demographics.query.filter_by(student_id=current_user.id).first()
        if demographics:
            form.demographicid.data = demographics.id
        form2 = None
    else:
        demographics = None
        form2 = StudentListForm()
        if form2.validate_on_submit():
            demographics = form2.name.data
            form.demographicid.data = demographics.id
            return render_template('add_to_corpus.html', demographics=demographics, form=form)

    if form.validate_on_submit():
        demographics = Demographics.query.get(form.demographicid.data)
        corpus = Corpus(demographics=demographics, title=form.title.data,
                        draft=form.draft.data, papertype=form.papertype.data)
        uploaded_file = form.original.data
        if uploaded_file:
            filename = secure_filename(uploaded_file.filename)
            full_filename = os.path.join(app.instance_path, filename)
            uploaded_file.save(full_filename)
            # TODO - process uploaded file
            corpus.text, corpus.num_words = doctools.doctools(filename=full_filename, first=demographics.student.first_name, last=demographics.student.last_name)
        else:
            corpus.text = form.text.data

        db.session.add(corpus)
        db.session.commit()
        flash(f'You have successfully uploaded your paper: {form.title.data}', 'info')
        return redirect(url_for('corpus_add'))

    return render_template('add_to_corpus.html', demographics=demographics, form=form, form2=form2)


@app.route('/corpus')
@login_required
def corpus_view():
    if not check_access(current_user, TEACHER_ACCESS):
        flash(f'You do not have access to query the corpus.', 'warning')
        return redirect(url_for('index'))

    texts = Corpus.query.order_by(Corpus.title).all()
    return render_template('corpus.html', texts=texts)


@app.route('/corpus/<int:id>')
@login_required
def corpus_text(id):
    if not check_access(current_user, TEACHER_ACCESS):
        flash(f'You do not have access to query the corpus.', 'warning')
        return redirect(url_for('index'))

    item = Corpus.query.get_or_404(id)
    item.text = item.text.replace(r"\n", "<br>")
    item.text = item.text.replace(r"\t", "")
    return render_template('corpus_text.html', item=item, demographics=item.demographics)


@app.route('/info/edit', methods=['GET', 'POST'])
@login_required
def personal_info():
    if current_user.user_type == STUDENT:
        demographics = Demographics.query.filter_by(student_id=current_user.id).first()

        form = DemographicsForm()
        form.first_name.data = current_user.first_name
        form.last_name.data = current_user.last_name
        form.email.data = current_user.email
        if form.validate_on_submit():
            if not demographics:
                demographics = Demographics(student=current_user)

            demographics.country_lookup = form.country.data
            demographics.language_lookup = form.language.data
            demographics.major = form.major.data
            demographics.set_nativeness(form.language.data.code)
            db.session.add(demographics)
            db.session.commit()
            flash(f'You have successfully updated your personal information', 'info')
            return redirect(url_for('index'))

    return render_template('demographics_form.html', form=form)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def server_error(e):
    return render_template('500.html', error=e), 500


def check_access(user, access_level):
    return int(user.access.name) >= int(access_level)