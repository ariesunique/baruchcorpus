from app import db, login

from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.functions import current_timestamp

from flask_login import UserMixin

from werkzeug.security import generate_password_hash, check_password_hash


Base = declarative_base()
metadata = Base.metadata


class AcademicLevelLookup(db.Model):
    __tablename__ = 'academic_level_lookup'

    code = Column(String(2), primary_key=True)
    name = Column(String(45), nullable=False)

    def __repr__(self):
        return f"<AcademicLevelLookup {self.code}: {self.name}>"


class AccessLevel(db.Model):
    __tablename__ = 'access_level'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False)
    description = Column(String(75), nullable=False)

    def __repr__(self):
        return f"<AccessLevel {self.name}: {self.description}>"

class Corpus(db.Model):
    __tablename__ = 'corpus'

    id = Column(Integer, primary_key=True)
    creator = Column(ForeignKey(u'demographics.id'), nullable=False, index=True)
    title = Column(String(128), nullable=False)
    text = Column(Text, nullable=False)
    draft = Column(Integer, nullable=False)
    num_words = Column(Integer)
    papertype_id = Column(ForeignKey(u'papertype_lookup.id'), nullable=False, index=True)
    uploaded_time = Column(DateTime, nullable=False, server_default=current_timestamp())
    #uploaded_filename = Column(String(128), nullable=False)

    demographics = relationship(u'Demographics')
    papertype = relationship(u'PapertypeLookup')

    def __repr__(self):
        return f"<Corpus {self.id}: {self.title} - {self.uploaded_time}>"


class CountryLookup(db.Model):
    __tablename__ = 'country_lookup'

    code = Column(String(2), primary_key=True)
    name = Column(String(45), nullable=False)

    def __repr__(self):
        return f"<CountryLookup {self.code}: {self.name}>"


class Demographics(db.Model):
    __tablename__ = 'demographics'

    id = Column(Integer, primary_key=True)
    student_id = Column(ForeignKey(u'users.id'), nullable=False, index=True)
    language_code = Column(ForeignKey(u'language_lookup.code'), nullable=False, index=True)
    country_code = Column(ForeignKey(u'country_lookup.code'), nullable=False, index=True)
    major_id = Column(ForeignKey(u'major_lookup.id'), nullable=False, index=True)
    is_native = Column(Integer, nullable=False, server_default=text("'1'"))

    country_lookup = relationship(u'CountryLookup')
    language_lookup = relationship(u'LanguageLookup')
    major = relationship(u'MajorLookup')
    student = relationship(u'User')

    def set_nativeness(self, language):
        print(f"Language is {language}")
        if language == "en":
            self.is_native = 1
        else:
            self.is_native = 0

    def __repr__(self):
        return f"<Demographics {self.student.username}: {self.country_lookup.name} {self.language_lookup.name}>"


class LanguageLookup(db.Model):
    __tablename__ = 'language_lookup'

    code = Column(String(2), primary_key=True)
    name = Column(String(45), nullable=False)

    def __repr__(self):
        return f"<LanguageLookup {self.code}: {self.name}>"


class MajorLookup(db.Model):
    __tablename__ = 'major_lookup'

    id = Column(Integer, primary_key=True)
    code = Column(String(4), nullable=False)
    name = Column(String(45), nullable=False)

    def __repr__(self):
        return f"<MajorLookup {self.code}: {self.name}>"


class PapertypeLookup(db.Model):
    __tablename__ = 'papertype_lookup'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False)
    description = Column(String(45), nullable=False)

    def __repr__(self):
        return f"<PapertypeLookup {self.name}: {self.description}>"


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(60), nullable=False, unique=True)
    email = Column(String(45))
    password_hash = Column(String(128), nullable=False)
    first_name = Column(String(45), nullable=False)
    last_name = Column(String(45), nullable=False)
    user_type = Column(String(1), nullable=False)
    access_level = Column(ForeignKey(u'access_level.id'), nullable=False, index=True)

    access = relationship(u'AccessLevel')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return f"<User {self.username} {self.email} {self.user_type}>"


@login.user_loader
def load_user(id):
    return User.query.get(id)