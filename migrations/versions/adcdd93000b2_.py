"""empty message

Revision ID: adcdd93000b2
Revises: 
Create Date: 2018-06-10 23:31:06.413715

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'adcdd93000b2'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('academic_level_lookup',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=45), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('country_lookup',
    sa.Column('code', sa.String(length=2), nullable=False),
    sa.Column('name', sa.String(length=45), nullable=False),
    sa.PrimaryKeyConstraint('code')
    )
    op.create_table('language_lookup',
    sa.Column('code', sa.String(length=2), nullable=False),
    sa.Column('name', sa.String(length=45), nullable=False),
    sa.PrimaryKeyConstraint('code')
    )
    op.create_table('major_lookup',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=4), nullable=False),
    sa.Column('name', sa.String(length=45), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('papertype_lookup',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=45), nullable=False),
    sa.Column('description', sa.String(length=45), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=60), nullable=False),
    sa.Column('email', sa.String(length=45), nullable=False),
    sa.Column('password_hash', sa.String(length=128), nullable=False),
    sa.Column('first_name', sa.String(length=45), nullable=False),
    sa.Column('last_name', sa.String(length=45), nullable=False),
    sa.Column('user_type', sa.String(length=1), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('username')
    )
    op.create_table('demographics',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('student_id', sa.Integer(), nullable=False),
    sa.Column('language_code', sa.String(length=2), nullable=True),
    sa.Column('country_code', sa.String(length=2), nullable=True),
    sa.Column('major_id', sa.Integer(), nullable=False),
    sa.Column('is_native', sa.Integer(), server_default=sa.text("'1'"), nullable=False),
    sa.ForeignKeyConstraint(['country_code'], ['country_lookup.code'], ),
    sa.ForeignKeyConstraint(['language_code'], ['language_lookup.code'], ),
    sa.ForeignKeyConstraint(['major_id'], ['major_lookup.id'], ),
    sa.ForeignKeyConstraint(['student_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_demographics_country_code'), 'demographics', ['country_code'], unique=False)
    op.create_index(op.f('ix_demographics_language_code'), 'demographics', ['language_code'], unique=False)
    op.create_index(op.f('ix_demographics_major_id'), 'demographics', ['major_id'], unique=False)
    op.create_index(op.f('ix_demographics_student_id'), 'demographics', ['student_id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_demographics_student_id'), table_name='demographics')
    op.drop_index(op.f('ix_demographics_major_id'), table_name='demographics')
    op.drop_index(op.f('ix_demographics_language_code'), table_name='demographics')
    op.drop_index(op.f('ix_demographics_country_code'), table_name='demographics')
    op.drop_table('demographics')
    op.drop_table('users')
    op.drop_table('papertype_lookup')
    op.drop_table('major_lookup')
    op.drop_table('language_lookup')
    op.drop_table('country_lookup')
    op.drop_table('academic_level_lookup')
    # ### end Alembic commands ###
